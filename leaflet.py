#!/usr/bin/env python
import os
from gimpfu import gimp, main, pdb, PF_IMAGE, PF_DRAWABLE, PF_DIRNAME, PF_SPINNER, register

BOX = 256
DIMENSIONS = (256, 512, 1024, 2048, 4096, 8192, 16384)
ZOOM_DEFAULT = 4
ZOOM_OPTIONS = (0, 8, 1)


def prepare_image(image, layer, dimension):
    """
    Prepare the image by scaling to the adequate resolution.
    """
    gimp.message('Preparing the image...')
    width = layer.width
    height = layer.height
    if width < height:
        new_width = dimension
        new_height = new_width * height / width
    else:
        new_height = dimension
        new_width = new_height * width / height
    pdb.gimp_image_scale(image, new_width, new_height)
    pdb.gimp_image_crop(image, dimension, dimension, 0, 0)
    gimp.message('Image prepared.')


def get_output_path(output_dir, z, x, y):
    """
    Create the path for saving an image. If the path folders don't exist, create them as well.
    """
    output_path = os.path.join(output_dir, str(z), str(x), str(y) + '.jpeg')
    output_directory = os.path.dirname(output_path)
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)
    return output_path


def create_tile(image, z, x, y, output_dir):
    """
    Create a tile in the calculated coordinates and save it to a disk.
    """
    gimp.message('Creating a tile...')
    new_image = image.duplicate()
    offset_x = x * BOX
    offset_y = y * BOX
    pdb.gimp_image_crop(new_image, BOX, BOX, offset_x, offset_y)
    output_path = get_output_path(output_dir, z, x, y)
    pdb.file_jpeg_save(new_image, new_image.active_layer, output_path, output_path, 0.9, 0, 0, 0,
                       'Creating with GIMP', 0, 0, 0, 0)
    pdb.gimp_image_delete(new_image)


def leaflet_tile(image, layer, output_dir, zoom_level):
    """
    Tiles the image for use in Leaflet maps.

    Parameters:
    output_dir : string The folder in which to save the produced tiles.
    """
    zoom_level = int(zoom_level)
    dimension = pow(2, zoom_level + 8)
    prepare_image(image, layer, dimension)
    for z in xrange(zoom_level, -1, -1):
        for x in xrange(dimension / BOX):
            for y in xrange(dimension / BOX):
                create_tile(image, z, x, y, output_dir)
        gimp.message('Zooming out...')
        dimension /= 2
        pdb.gimp_image_scale(image, dimension, dimension)
    gimp.message('Done!')

register(
    'Leafletize',
    'Tile current image',
    'Tiles the image and saves the output ready for use in leaflet maps',
    'Zlatko Masek',
    'GNU GPL v3',
    '2016',
    '<Image>/Filters/Leaflet/Tile',
    '*',
    [
        (PF_IMAGE, 'image', 'Input image', None),
        (PF_DRAWABLE, 'drawable', 'Input drawable', None),
        (PF_DIRNAME, 'output_dir', 'Output directory', os.path.realpath(os.curdir)),
        (PF_SPINNER, 'zoom_level', 'Zoom level', ZOOM_DEFAULT, ZOOM_OPTIONS)
    ],
    [],
    leaflet_tile,
    menu='<Image>/Filters/Leaflet/Tile'
)

main()
